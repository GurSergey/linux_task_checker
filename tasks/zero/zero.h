//
// Created by serge on 20.09.2021.
//

#include <stdlib.h>
#include "../task.h"

#ifndef LINUX_TASK_CHECKER_ZERO_H
#define LINUX_TASK_CHECKER_ZERO_H

const struct Task zero = {
        // commands for checking
        {"uname\n", "ls | grep 1.txt\n"},
        // right answer
        {"Linux\n", "1.txt\n"},
        // prompts for task
        {"Your OS is not Linux", "File 1.txt is not found in your folder"},
        // secret strings
        "42"
};
#endif //LINUX_TASK_CHECKER_ZERO_H
