USER_ID_LMS=2 && cc main.c task_list.h tasks/zero/zero.h \
  tasks/zero/zero.c tasks/task.h \
  tasks/logos/haulmont.h tasks/logos/tux.h \
  integration_lms.c config.h debug_info.h \
  tasks/easter/easter_main.c tasks/easter/easter_main.h \
  tasks/easter/2048.h tasks/easter/2048.c integration_lms.h \
  tasks/easter/cmatrix.c tasks/easter/cmatrix.h \
  tasks/easter/tictactoe.c tasks/easter/tictactoe.h \
  tasks/easter/main_blackjack.c tasks/easter/blackjack.h \
  tasks/easter/blackjack.c tasks/easter/main_blackjack.h \
  tasks/easter/minisweeper.c tasks/easter/minisweeper_def.h \
  tasks/easter/minisweeper_aux.c tasks/easter/minisweeper_menu.c \
  tasks/easter/minisweeper_score.c tasks/easter/minisweeper.h \
  tasks/easter/tetris.h tasks/easter/tetris.c \
  tasks/easter/main_snake.c tasks/easter/snake.c \
  tasks/easter/snake.h tasks/easter/main_snake.h -o linux_task_checker \
  -lcurl -lncurses -D"USER_ID_LMS=${USER_ID_LMS}"
