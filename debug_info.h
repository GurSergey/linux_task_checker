//
// Created by serge on 03.10.2021.
//

#include "config.h"

#ifndef LINUX_TASK_CHECKER_DEBUG_INFO_H
#define LINUX_TASK_CHECKER_DEBUG_INFO_H

#define DEBUG_NUM -10

char* debug_info = "This is debug information about task checker \n"
             "Metodhist of Linux course: Alexander Sharabarin \n"
             "Authors: Sergey Gurylev \n\n"
             "Information build \n"
             "Version of program: 0.5 \n"
             "Lms integration include: " WITH_LMS_CHECKING "\n"
             "User id in LMS: " USER_ID "\n"
             "User name in LMS: " USER_NAME "\n"
             "Id course in LMS: " COURSE_ID "\n\n"
             "About \n"
             "This program developed for automation checking tasks Linux Haulmont course \n"
             "You can send feedback to emails: gyrsergey@yandex.ru, gyrsergey@gmail.com \n"
             "Or skype login: gyrsergey";

#endif //LINUX_TASK_CHECKER_DEBUG_INFO_H
