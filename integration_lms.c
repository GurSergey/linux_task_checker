//
// Created by serge on 02.10.2021.
//

#include <stdio.h>
#include <curl/curl.h>
#include "config.h"

#define OK_STATUS 0

int setInMarkInLms() {
    CURL *curl;
    CURLcode res;
    int status = -1;
    curl_global_init(CURL_GLOBAL_ALL);
    /* get a curl handle */
    curl = curl_easy_init();
    if(curl) {
        /* First set the URL that is about to receive our POST. This URL can
           just as well be a https:// URL if that is what should receive the
           data. */
        curl_easy_setopt(curl, CURLOPT_URL, "http://postit.example.com/");
        /* Now specify the POST data */
//        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "name=daniel&project=curl");
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_BASIC);
        curl_easy_setopt(curl, CURLOPT_USERNAME, LOGIN_ROBOT);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, PASS_ROBOT);
        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);
        /* Check for errors */

        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
        if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK) {
            status = OK_STATUS;
        } else {

        }
        /* always cleanup */
        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();
    return status;
}