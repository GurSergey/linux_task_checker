//
// Created by serge on 20.09.2021.
//

#ifndef LINUX_TASK_CHECKER_TASK_LIST_H
#define LINUX_TASK_CHECKER_TASK_LIST_H

#include "tasks/zero/zero.h"

static const char task_list[] = "0 - First test task check\n";

static struct Task tasks[] = {
        [0] = zero
};
#endif //LINUX_TASK_CHECKER_TASK_LIST_H
